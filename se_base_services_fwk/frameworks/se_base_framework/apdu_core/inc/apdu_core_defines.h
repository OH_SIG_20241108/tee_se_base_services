/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CORE_INC_APDU_CORE_DEFINES_H
#define CORE_INC_APDU_CORE_DEFINES_H

#include <stdint.h>

#include "se_base_services_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_APDU_DATA_SIZE 0xFF
#define MAX_APDU_RESP_SIZE 0x100
#define MIN_APDU_RESP_SIZE 0x2

#define AID_LENGTH_MIN 4U
#define AID_LENGTH_MAX 16U

enum CmdClass {
    CLA_DEFAULT = 0x80,
};

enum StatusWords {
    SW_NO_ERROR = 0x9000,
};

typedef struct AppIdentifier {
    uint8_t *aid;
    uint32_t aidLen;
} AppIdentifier;

#ifdef __cplusplus
}
#endif

#endif // CORE_INC_APDU_CORE_DEFINES_H