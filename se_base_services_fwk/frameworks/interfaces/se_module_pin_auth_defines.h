/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INTERFACES_MODULE_PIN_AUTH_DEFINES_H
#define INTERFACES_MODULE_PIN_AUTH_DEFINES_H

#include <stdint.h>

#include "se_base_services_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PIN_DATA_HASH_LEN 64
typedef struct PinDataHash {
    uint8_t data[PIN_DATA_HASH_LEN];
} PinDataHash;

#define PIN_DATA_SECRET_LEN 32
typedef struct PinDataSecret {
    uint8_t data[PIN_DATA_SECRET_LEN];
} PinDataSecret;

#define PIN_DATA_REMOTE_BASE_LEN 64
typedef struct PinDataRemoteBase {
    uint8_t data[PIN_DATA_REMOTE_BASE_LEN];
} PinDataRemoteBase;

#define PIN_DATA_REMOTE_PUBLIC_KEY_LEN 64
typedef struct PinDataRemotePublicKey {
    uint8_t data[PIN_DATA_REMOTE_PUBLIC_KEY_LEN];
} PinDataRemotePublicKey;

#define PIN_DATA_REMOTE_NONCE_LEN 16
typedef struct PinDataRemoteNonce {
    uint8_t data[PIN_DATA_REMOTE_NONCE_LEN];
} PinDataRemoteNonce;

#define PIN_DATA_REMOTE_HMAC_LEN 32
typedef struct PinDataRemoteHmac {
    uint8_t data[PIN_DATA_REMOTE_HMAC_LEN];
} PinDataRemoteHmac;

typedef struct PinDataRemoteServiceChallenge {
    PinDataRemotePublicKey pk;
    PinDataRemoteNonce nonce;
} PinDataRemoteServiceChallenge;

typedef struct PinDataRemoteClientProof {
    PinDataRemotePublicKey pk;
    PinDataRemoteNonce nonce;
    PinDataRemoteHmac hmac;
} PinDataRemoteClientProof;

typedef enum PinAuthStatus {
    SE_PIN_AUTH_STATUS_OK = 0x00,
    SE_PIN_AUTH_STATUS_FAIL = 0x01,
    SE_PIN_AUTH_STATUS_PUNISH = 0x02,
    SE_PIN_AUTH_STATUS_DESTRUCT = 0x03,
} PinAuthStatus;

typedef enum PinConfigStatus {
    SE_PIN_CONFIG_STATUS_OK = 0x00,
    SE_PIN_CONFIG_STATUS_FAIL_NO_PERM = 0x07,
    SE_PIN_CONFIG_STATUS_FAIL_AUTH_ERR = 0x08,
} PinConfigStatus;

typedef struct PinFreezeStatus {
    uint16_t failCnt;
    uint16_t destructCnt;
    uint32_t punishTime;
} PinFreezeStatus;

typedef struct PinAuthResult {
    PinAuthStatus status;
    PinFreezeStatus freeze;
    PinDataSecret secret;
} PinAuthResult;

typedef struct PinDestructConfig {
    uint32_t enable;
    uint16_t destructMaxCnt;
} PinDestructConfig;

typedef struct PinConfigResult {
    PinConfigStatus status;
    PinFreezeStatus freeze;
} PinConfigResult;

#ifdef __cplusplus
}
#endif

#endif // INTERFACES_MODULE_PIN_AUTH_DEFINES_H