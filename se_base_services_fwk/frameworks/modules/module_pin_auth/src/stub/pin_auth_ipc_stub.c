/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "pin_auth_ipc_stub.h"

#include <stddef.h>

#include "logger.h"
#include "module_common_ipc_stub.h"
#include "pin_auth_ipc_defines.h"
#include "pin_auth_ipc_stub_inner.h"
#include "se_module_pin_auth_defines.h"

typedef ResultCode ProcPinAuthStubProcess(PinAuthContext *context, SharedDataBuffer *buffer);

typedef struct PinAuthStubDispatch {
    uint32_t cmd;
    ProcPinAuthStubProcess *func;
} PinAuthStubDispatch;

ResultCode ProcessPinAuthCommandStub(PinAuthContext *context, uint32_t cmd, SharedDataBuffer *buffer)
{
    if (context == NULL || buffer == NULL) {
        LOG_ERROR("invalid context or shared buffer");
        return INVALID_PARA_NULL_PTR;
    }

    static const PinAuthStubDispatch DISPATCHS[] = {
        {CMD_PIN_AUTH_GET_SLOT_NUM, ProcPinAuthCmdGetNumSlotsStub},
        {CMD_PIN_AUTH_ENROLL_LOCAL, ProcPinAuthCmdEnrollLocalStub},
        {CMD_PIN_AUTH_AUTHENTICATION_LOCAL, ProcPinAuthCmdAuthLocalStub},
        {CMD_PIN_AUTH_ENROLL_REMOTE, ProcPinAuthCmdEnrollRemoteStub},
        {CMD_PIN_AUTH_AUTHENTICATION_REMOTE_PREPARE, ProcPinAuthCmdAuthRemotePrepareStub},
        {CMD_PIN_AUTH_AUTHENTICATION_REMOTE, ProcPinAuthCmdAuthRemoteStub},
        {CMD_PIN_AUTH_AUTHENTICATION_REMOTE_ABORT, ProcPinAuthCmdAuthRemoteAbortStub},
        {CMD_PIN_AUTH_CONFIG_SELF_DESTRUCT, ProcPinAuthCmdSetSelfDestructEnableStub},
        {CMD_PIN_AUTH_GET_SLOT_STATUS, ProcPinAuthCmdGetFreezeStatusStub},
        {CMD_PIN_AUTH_CONFIG_FREEZE_POLICY, ProcPinAuthCmdSetFreezePolicyStub},
        {CMD_PIN_AUTH_GET_FREEZE_POLICY, ProcPinAuthCmdGetFreezePolicyStub},
        {CMD_PIN_AUTH_ERASE_SINGLE_SLOT, ProcPinAuthCmdEraseSingleSlotStub},
        {CMD_PIN_AUTH_ERASE_ALL_SLOTS, ProcPinAuthCmdEraseAllSlotsStub},
    };

    LOG_INFO("invoked, cmd=0x%x, dSize=%u, dMaxSize=%u", cmd, buffer->dataSize, buffer->dataMaxSize);

    for (uint32_t loop = 0; loop < sizeof(DISPATCHS) / sizeof(PinAuthStubDispatch); loop++) {
        const PinAuthStubDispatch *dispatch = &DISPATCHS[loop];
        if (dispatch->cmd == cmd && dispatch->func != NULL) {
            return dispatch->func(context, buffer);
        }
    }

    return ProcessSeCommonCommandStub(&context->base, cmd, buffer);
}
