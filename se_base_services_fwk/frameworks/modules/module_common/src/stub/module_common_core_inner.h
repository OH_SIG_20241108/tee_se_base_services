/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_COMMON_CORE_INNER_H
#define CODE_MODULES_COMMON_CORE_INNER_H

#include "module_common_core.h"

#ifdef __cplusplus
extern "C" {
#endif

// 2-bytes version, 4-byte vendor, 12-bytes config, 8-bytes status, 2-bytes sw.
#define SE_APP_SELECT_MAX_RSP_LENGTH 28

#define FLAG_NO_LOCK 0
#define FLAG_LOCK 2

ResultCode SeServiceStatusFromRspApdu(const uint8_t *response, uint32_t length, SeServiceStatus *output);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_COMMON_CORE_INNER_H