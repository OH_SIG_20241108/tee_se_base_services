/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_COMMON_CORE_H
#define CODE_MODULES_COMMON_CORE_H

#include "card_channel.h"
#include "se_base_services_defines.h"
#include "se_module_common_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

ResultCode SeCommonIsServiceAvailable(CardChannel *channel, ServiceId sid, SeServiceStatus *status);

ResultCode SeCommonSetServiceConfiguration(CardChannel *channel, ServiceId sid, uint32_t flag,
    const SeServiceConfig *config);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_COMMON_CORE_H