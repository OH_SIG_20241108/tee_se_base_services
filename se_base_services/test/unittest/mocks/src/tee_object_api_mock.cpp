/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tee_hw_ext_api.h"

#include <new>

#include <securec.h>

extern "C" {
TEE_Result TEE_AllocateTransientObject(uint32_t objectType, uint32_t maxObjectSize, TEE_ObjectHandle *object)
{
    if (object == nullptr) {
        return TEE_FAIL;
    }
    (void)objectType;
    (void)maxObjectSize;

    auto *handle = new TEE_ObjectHandleInner();
    if (handle == nullptr) {
        return TEE_FAIL;
    }

    *object = handle;
    return TEE_SUCCESS;
}

void TEE_InitRefAttribute(TEE_Attribute *attr, uint32_t attributeID, uint8_t *buffer, uint32_t length)
{
    (void)attr;
    (void)attributeID;
    (void)buffer;
    (void)length;
}

void TEE_FreeTransientObject(TEE_ObjectHandle object)
{
    if (object == nullptr) {
        return;
    }
    delete object;
}

TEE_Result TEE_PopulateTransientObject(TEE_ObjectHandle object, TEE_Attribute *attrs, uint32_t attrCount)
{
    (void)object;
    (void)attrs;
    (void)attrCount;
    return TEE_SUCCESS;
}

TEE_Result tee_obj_init(void)
{
    return TEE_SUCCESS;
}
}