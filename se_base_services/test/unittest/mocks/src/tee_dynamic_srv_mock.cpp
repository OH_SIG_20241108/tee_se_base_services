/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tee_dynamic_srv_mock.h"

#include <securec.h>

extern "C" {
TEE_Result tee_srv_get_uuid_by_sender(uint32_t sender, TEE_UUID *uuid)
{
    if (auto *invoker = OHOS::SeBaseServices::UnitTest::TeeDynamicSrvMock::GetInstance(); invoker) {
        return invoker->tee_srv_get_uuid_by_sender(sender, uuid);
    }
    *uuid = {0xabc29315, 0xcccd, 0x4dc0, {0xac, 0xe9, 0xd7, 0x9e, 0xa1, 0x51, 0x56, 0x25}};
    return 0;
}

void tee_srv_unmap_from_task(uint32_t vaAddr, uint32_t size)
{
        if (auto *invoker = OHOS::SeBaseServices::UnitTest::TeeDynamicSrvMock::GetInstance(); invoker) {
        return invoker->tee_srv_unmap_from_task(vaAddr, size);
        }
}

int tee_srv_map_from_task(uint32_t inTaskId, uint32_t vaAddr, uint32_t size, uint32_t *virtAddr)
{
    if (auto *invoker = OHOS::SeBaseServices::UnitTest::TeeDynamicSrvMock::GetInstance(); invoker) {
        return invoker->tee_srv_map_from_task(inTaskId, vaAddr, size, virtAddr);
    }
    return 0;
}

void tee_srv_cs_server_loop(const char *taskName, const struct srv_dispatch_t *dispatch, uint32_t dispatchSize,
    struct srv_thread_init_info *curThread)
{
    if (auto *invoker = OHOS::SeBaseServices::UnitTest::TeeDynamicSrvMock::GetInstance(); invoker) {
            return invoker->tee_srv_cs_server_loop(taskName, dispatch, dispatchSize, curThread);
    }
}
}