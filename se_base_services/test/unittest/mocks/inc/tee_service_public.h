/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNIT_TEST_INC_TEE_SERVICE_PUBLIC_H
#define UNIT_TEST_INC_TEE_SERVICE_PUBLIC_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif

#include <stdlib.h>

#include "tee_defines.h"

/* don't allow to edit these files */
#define TEE_SERVICE_MSG_QUEUE_SIZE 100
typedef struct {
    uint64_t arg0;
    uint64_t arg1;
    uint64_t arg2;
    uint64_t arg3;
    uint64_t arg4;
    uint64_t arg5;
    uint64_t arg6;
    uint64_t arg7;
} args_t;

typedef union {
    args_t args_data;
} tee_service_ipc_msg;

typedef struct {
    TEE_Result ret;
    tee_service_ipc_msg msg;
} tee_service_ipc_msg_rsp;

#ifdef __cplusplus
extern "C" {
#endif

void tee_common_ipc_proc_cmd(const char *taskName, uint32_t sndCmd, const tee_service_ipc_msg *sndMsg, uint32_t ackCmd,
    tee_service_ipc_msg_rsp *rspMsg);

#ifdef __cplusplus
}

#endif
#endif // UNIT_TEST_INC_TEE_SERVICE_PUBLIC_H