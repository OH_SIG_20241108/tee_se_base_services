/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include <securec.h>

#include "dyn_services_core_inner.h"
#include "tee_dynamic_srv_mock.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
using namespace testing;

TEST(ServiceSeCardChannelTest, GetRequestCommand)
{
    tee_service_ipc_msg msg;
    (void)memset_s(&msg, sizeof(tee_service_ipc_msg), 0, sizeof(tee_service_ipc_msg));
    msg.args_data.arg0 = 5242882;
    EXPECT_EQ(0x50, GetRequestServiceId(&msg));
    EXPECT_EQ(0x500002, GetRequestCommand(&msg));
}

TEST(ServiceSeCardChannelTest, CheckClientPermissionNullptr)
{
    EXPECT_NE(SUCCESS, CheckClientPermission(0, nullptr, 0));
};

TEST(ServiceSeCardChannelTest, CheckClientPermissionCheckWillFailed)
{
    TEE_UUID uuid1 = {0x11111111, 0x2d2e, 0x4c3d, {0x8c, 0x3f, 0x34, 0x99, 0x78, 0x3c, 0xa9, 0x73}};
    TEE_UUID uuid2 = {0x22222222, 0x2d2e, 0x4c3d, {0x8c, 0x3f, 0x34, 0x99, 0x78, 0x3c, 0xa9, 0x73}};
    TEE_UUID uuid3 = {0x33333333, 0x2d2e, 0x4c3d, {0x8c, 0x3f, 0x34, 0x99, 0x78, 0x3c, 0xa9, 0x73}};
    const TEE_UUID list[] = {uuid1, uuid2, uuid3};

    EXPECT_NE(SUCCESS, CheckClientPermission(0, list, sizeof(list) / sizeof(TEE_UUID)));
};

TEST(ServiceSeCardChannelTest, CheckClientPermissionCheckWillSuccessSingle)
{
    constexpr TEE_UUID uuid1 = {0x12345678, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};
    constexpr TEE_UUID list[] = {uuid1};

    TeeDynamicSrvMock mock;
    EXPECT_CALL(mock, tee_srv_get_uuid_by_sender).WillOnce(Invoke([&uuid1](uint32_t, TEE_UUID *uuid) {
        *uuid = uuid1;
        return 0;
    }));

    EXPECT_EQ(SUCCESS, CheckClientPermission(0, list, sizeof(list) / sizeof(TEE_UUID)));
};

TEST(ServiceSeCardChannelTest, CheckClientPermissionCheckWillSuccessMutiul)
{
    constexpr TEE_UUID uuid1 = {0x11111111, 0x2d2e, 0x4c3d, {0x8c, 0x3f, 0x34, 0x99, 0x78, 0x3c, 0xa9, 0x73}};
    constexpr TEE_UUID uuid2 = {0x22222222, 0x2d2e, 0x4c3d, {0x8c, 0x3f, 0x34, 0x99, 0x78, 0x3c, 0xa9, 0x73}};
    constexpr TEE_UUID uuid3 = {0x33333333, 0x2d2e, 0x4c3d, {0x8c, 0x3f, 0x34, 0x99, 0x78, 0x3c, 0xa9, 0x73}};
    constexpr TEE_UUID uuid4 = {0x12345678, 0x0000, 0x0000, {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};
    constexpr TEE_UUID list[] = {uuid1, uuid2, uuid3, uuid4};

    TeeDynamicSrvMock mock;
    EXPECT_CALL(mock, tee_srv_get_uuid_by_sender).WillOnce(Invoke([&uuid4](uint32_t, TEE_UUID *uuid) {
        *uuid = uuid4;
        return 0;
    }));

    EXPECT_EQ(SUCCESS, CheckClientPermission(0, list, sizeof(list) / sizeof(TEE_UUID)));
};
} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS