/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <tee_defines.h>
#include <tee_dynamic_srv.h>

#include "dyn_services_core_inner.h"
#include "dyn_services_log.h"
#include "logger.h"

PUBLIC_API void tee_task_entry(void)
{
    ServiceLoggerInit();
    tee_obj_init();
    const static struct srv_dispatch_t disp = {
        .cmd = 0,
        .fn = ServiceProcessWithCommand,
    };

    tee_srv_cs_server_loop(SERVICE_NAME, &disp, 1, NULL);
}
