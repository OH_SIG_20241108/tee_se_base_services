/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "client_pin_auth.h"

#include <securec.h>

#include "client_base.h"
#include "pin_auth_ipc_proxy.h"

PUBLIC_API ResultCode SePinEnrollLocal(uint32_t slotId, const PinDataHash *hash, PinDataSecret *secret)
{
    return SePinEnrollLocalProxy(SendRequestToServer, slotId, hash, secret);
}

PUBLIC_API ResultCode SePinEnrollRemote(uint32_t slotId, const PinDataRemoteBase *base)
{
    return SePinEnrollRemoteProxy(SendRequestToServer, slotId, base);
}

PUBLIC_API ResultCode SePinAuthLocal(uint32_t slotId, const PinDataHash *hash, PinAuthResult *result)
{
    return SePinAuthLocalProxy(SendRequestToServer, slotId, hash, result);
}

PUBLIC_API ResultCode SePinSetSelfDestructEnable(uint32_t slotId, const PinDestructConfig *config,
    const PinDataHash *hash, PinConfigResult *configResult)
{
    return SePinSetSelfDestructEnableProxy(SendRequestToServer, slotId, config, hash, configResult);
}

PUBLIC_API ResultCode SePinGetFreezeStatus(uint32_t slotId, PinFreezeStatus *pinFreezeStatus)
{
    return SePinGetFreezeStatusProxy(SendRequestToServer, slotId, pinFreezeStatus);
}

PUBLIC_API ResultCode SePinGetNumSlots(uint32_t *numSlots)
{
    return SePinGetNumSlotsProxy(SendRequestToServer, numSlots);
}

PUBLIC_API ResultCode SePinSetFreezePolicy(uint16_t punishStartCnt, uint16_t destructMaxCnt,
    uint32_t enableDestructDefault)
{
    return SePinSetFreezePolicyProxy(SendRequestToServer, punishStartCnt, destructMaxCnt, enableDestructDefault);
}

PUBLIC_API ResultCode SePinGetFreezePolicy(uint16_t *punishStartCnt, uint16_t *destructMaxCnt,
    uint32_t *enableDestructDefault)
{
    return SePinGetFreezePolicyProxy(SendRequestToServer, punishStartCnt, destructMaxCnt, enableDestructDefault);
}

PUBLIC_API ResultCode SePinAuthRemotePrepare(uint32_t slotId, uint64_t session,
    PinDataRemoteServiceChallenge *challenge)
{
    return SePinAuthRemotePrepareProxy(SendRequestToServer, slotId, session, challenge);
}

PUBLIC_API ResultCode SePinAuthRemote(uint32_t slotId, uint64_t session, const PinDataRemoteClientProof *proof,
    PinAuthResult *result)
{
    return SePinAuthRemoteProxy(SendRequestToServer, slotId, session, proof, result);
}

PUBLIC_API ResultCode SePinAuthRemoteAbort(uint32_t slotId, uint64_t session)
{
    return SePinAuthRemoteAbortProxy(SendRequestToServer, slotId, session);
}

PUBLIC_API ResultCode SePinEraseSingleSlot(uint32_t slotId)
{
    return SePinEraseSingleSlotProxy(SendRequestToServer, slotId);
}

PUBLIC_API ResultCode SePinEraseAllSlots(void)
{
    return SePinEraseAllSlotsProxy(SendRequestToServer);
}